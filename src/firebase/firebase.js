import firebase from "firebase/app";
import "firebase/firestore";
import 'firebase/storage';  

const config = {
    apiKey: "AIzaSyBWLgA20Y82sRPHmACm0LHBnGSNdTTYHM0",
    authDomain: "helpoprototypeserver.firebaseapp.com",
    databaseURL: "https://helpoprototypeserver-default-rtdb.firebaseio.com",
    projectId: "helpoprototypeserver",
    storageBucket: "helpoprototypeserver.appspot.com",
    messagingSenderId: "726666511518",
    appId: "1:726666511518:web:bacefe49223b7cb4cfe78f",
    measurementId: "G-FNVJNJB35T"
};

export const fire = () => {
    if (!firebase.apps.length) {
        firebase.initializeApp(config);
    }
    return firebase.storage()
}