import React from 'react'
import lectureConfig from '../config/lectureConfig.json'
import '../style/Subconcept.css';

class Subconcept extends React.Component {
    constructor(props){
        super(props)
    }

    onClick(lectureCode, step, startTime){
        this.props.handler(lectureCode, step, startTime)    
    }

    makeSubconceptList(){
        return this.props.subconceptList.map((lectureInfo, key) => {
            const lectureCode = lectureInfo[0]
            const startTime = lectureInfo[1]
            const checkPoint = lectureConfig[lectureCode].checkPoint
            var step=-1
            for(var i in checkPoint){
                if(startTime>=checkPoint[i]){ 
                    step=-2 
                }
                if(step===-2 && startTime<checkPoint[i]){
                    step = i-1
                }
            }
            if(step == -2) {step = checkPoint.length-1}
            if(step == -1) {step = 0}
            return( 
                <button className="SButton" key={"subconceptList"+key} onClick={()=>this.onClick(lectureCode, step, startTime)}>
                    <div className="SButtonText">
                        {lectureCode}
                    </div>
                </button>
            )}
        )
    }

    render(){
        
        return (
        <div className="Subconcept">
            {this.makeSubconceptList()}
        </div>
        )
    }
}

export default Subconcept;