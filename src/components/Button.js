import React from 'react'
import '../style/Question.css';

class Button extends React.Component{
    render(){
        return(
            <button className="QButton" onClick={this.props.onClick}>
              <p className="QButtonText">
                {this.props.text}
              </p>
            </button>
        )
    }
}

export default Button