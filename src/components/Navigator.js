import React from 'react'
import '../style/Navigator.css';

class Navigator extends React.Component {
    constructor(props){
        super(props)
    }

    onClick(key){
        this.props.handler(key)    
    }

    makeNavigator(){
        return this.props.navigatorList.map((prevInfo, key) => {
                const lectureCode = prevInfo.lectureCode
                let navigatorList = [ 
                    <div className="NButton" key={"navigator"+key} onClick={()=>this.onClick(key)}>
                        <div className="NButtonText">
                            {lectureCode}
                        </div>
                    </div>
                ]
                if(key + 1 != this.props.navigatorList.length){
                    navigatorList.push(
                        <div className="NButtonText" style={{cursor: "default"}}>
                            {">"}
                        </div>
                    )
                }
                
                return navigatorList
            }
        )
    }

    render(){
        return (
            <div className="Navigator">
                {this.makeNavigator()}
            </div>
        );
    }
}

export default Navigator;