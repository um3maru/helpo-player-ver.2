import React, {Text} from 'react'
import '../style/Question.css';
import Button from './Button'

class Question extends React.Component {
  constructor(props){
    super(props)
    this.state={
        input:'', correct:null, showAnswer:false
    }
    this.donknow = this.donknow.bind(this)
    this.skip = this.skip.bind(this)
    this.replay = this.replay.bind(this)
    this.solveAgain = this.solveAgain.bind(this)
  }

  onSumbitResponse(){
    console.log(this.props.questionState)
    if(this.props.questionState !== 'unSubmit') {
      if(this.props.questionState=='correct'){
        return (
          <div className="ResultToast"
            style={{backgroundColor: "#14AE68", fontSize: 24, color: "white"}}>
            "정답입니다"
          </div>
          );
      }else if(this.props.questionState=='wrong'){
        return (
          <div className="ResultToast"
            style={{backgroundColor: "#ED4444", fontSize: 24, color: "white"}}>
            "오답입니다"
          </div>
          );
      }else{ return }
    }
  }

  onExplonation(){
    console.log(this.props.solutionState)
    if(this.props.solutionState === 'show'){
      return this.props.explonation
    }
    else return
  }

  onChange = (e) => {
    const value = e.target.value;
    this.setState({input:value})
  };
  
  donknow = () => {
    this.setState({showAnswer:true, correct:null, input:""})    
  }

  replay = () => {
    this.props.handler('replay')
  }

  skip = () => {
    this.props.handler('skip')
  }

  solveAgain = () => {
    this.setState({showAnswer:false})
  }

  input = () => {
    if(this.state.input === '') { return (
      <input
        name="name"
        placeholder="클릭해서 정답을 입력하세요"
        onChange={this.onChange}
        value={this.state.input}
        style={{margin: 20, backgroundColor: "rgba(0,0,0,0)", outline: "none", border: "none", width: 350, fontSize: 18}}
      />)
    }
    else { return (
      [<input
        name="name"
        placeholder="클릭해서 정답을 입력하세요"
        onChange={this.onChange}
        value={this.state.input}
        style={{margin: 20, backgroundColor: "rgba(0,0,0,0)", outline: "none", border: "none", width: 140, fontSize: 18}}
      />,
      <button className="QSubmit" onClick={() => this.props.onSubmit(this.state.input, this.props.answer)}>확인</button>]
      )
    }
  }
  
  buttonSet = () => {
    return (
      <div className="QSelections">
        <div className="QAnswer">
          {this.input()}
        </div>
        <Button onClick={this.props.onSolution} text={"문제해설 보기"}/>
        <Button onClick={this.replay} text={"내용 다시보기"} />
        <Button onClick={this.props.onStageChange} text={"step-by-step"} />
        <Button text={"넘어가기(사용불가)"} />
      </div>
    )
  }

  render(){
    if(!this.props.onQuestion) {return <div></div>}
    return (
      <div className="Question">
        <div className="QBox">
          <div className="QTitle">
            {"Title"}
          </div>
          <div className="QStep">
            {"문제: " + "n" + "단계"}
          </div>
          <div className="QText">
            {this.props.questionText}
          </div>
          <div className="QText" style={{color: "red"}}> 
            {this.onExplonation()}
          </div>
        </div>
        {this.buttonSet()}
        {this.onSumbitResponse()}
      </div>
    );
  }
}

export default Question;