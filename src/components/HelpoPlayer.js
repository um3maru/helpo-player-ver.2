import React from 'react'
import lectureConfig from '../config/lectureConfig.json'
import questionConfig from '../config/questionConfig.json'
import Navigator from './Navigator'
import Question from './Question'
import Subconcept from './Subconcept'
import HelpoVideo from './HelpoVideo'
import {fire} from '../firebase/firebase'
import '../style/HelpoPlayer.css';



class HelpoPlayer extends React.Component{
  constructor(props){
    super(props)
    this.pastVideoSrc = null
    this.videoSrc = null
    this.storage = fire()
    this.videoRef = React.createRef();
    this.startTime = 0
    this.state = {
      lectureCode:this.props.lectureCode, 
      onQuestion:false, 
      lectureStage:0, 
      questionStage:0, 
      questionState:"unsubmit",
      solutionState:"hide",
      currentTime:0,
      navigatorList:[]
    }

    this.questionResponseHandler = this.questionResponseHandler.bind(this)
    this.subconceptHandler = this.subconceptHandler.bind(this)
    this.navigatorHandler = this.navigatorHandler.bind(this)
  }

  questionResponseHandler(response){
    const checkPoint = lectureConfig[this.state.lectureCode].checkPoint
    const lectureStage = this.state.lectureStage
    switch ( response ) {
      case 'replay':
        if(lectureStage > 0 ){ this.videoRef.current.currentTime = checkPoint[lectureStage-1]+0.2 }
        else{this.videoRef.current.currentTime = 0 }
        this.videoRef.current.play()
        this.setState({onQuestion:false, solutionState:'hide'})  
        break;
      case 'skip':
        this.videoRef.current.currentTime = checkPoint[lectureStage]+0.2
        this.videoRef.current.play()
        this.setState({onQuestion:false, solutionState:'hide'})
        break;
      default:
    }
  }

  onStageChange = () =>{
    const checkPoint = lectureConfig[this.state.lectureCode].checkPoint
    const questions = questionConfig[this.state.lectureCode][this.state.lectureStage]
    const len = questions.length
    console.log(this.state.questionStage, len)
    
    if(this.state.questionStage<len-1){
      this.setState({questionStage:this.state.questionStage+1, solutionState:'hide'})
    }
    else{
      this.videoRef.current.currentTime = checkPoint[this.state.lectureStage]+0.2
      this.videoRef.current.play()
      this.setState({onQuestion:false, solutionState:'hide', questionStage:0})
    }
  }

  onSolution = () =>{
    this.setState({solutionState:'show'})
  }

  onSubmit = (answer, input) => {
    console.log(answer,input)
    if(answer === input) { 
      this.setState({questionState:'correct'}, 
        ()=>{setTimeout(()=>this.setState({questionState:'unSubmit'}), 3000);}   
      )
    }
    else { 
      this.setState({questionState:'wrong'},  
        ()=>{setTimeout(()=>this.setState({questionState:'unSubmit'}), 3000);}   
      )
    }
  }

  subconceptHandler(lectureCode, lectureStage, startTime){
    this.startTime = startTime
    this.setState(
      {
        lectureCode:lectureCode, 
        lectureStage:lectureStage, 
        onQuestion:false, 
        navigatorList:this.state.navigatorList.concat(
          {
            lectureCode:this.state.lectureCode, 
            lectureStage:this.state.lectureStage, 
            startTime:this.state.currentTime
          }
        )
      }
    )
  }

  navigatorHandler(n){
    //navigatorList의 n번째 요소 클릭
    console.log(n)
    const {startTime, lectureCode, lectureStage} = this.state.navigatorList[n]
    this.startTime = startTime
    this.setState({lectureCode:lectureCode, lectureStage:lectureStage, navigatorList:this.state.navigatorList.slice(0, n)})
  }

  

  playPauseRenderer(){
    if(this.videoRef.current != null){
      if (this.videoRef.current.paused || this.videoRef.current.ended) {
        document.getElementById('playpause').setAttribute('data-state', 'play');
      }
      else {
        document.getElementById('playpause').setAttribute('data-state', 'pause');
      }
    }
  }


  volumePressRenderer(){
    if(this.videoRef.current != null){
      if (this.videoRef.current.muted) {
        document.getElementById('mute').setAttribute('data-state', 'volume-off');
      }
      else {
        document.getElementById('mute').setAttribute('data-state', 'volume-on');
      }
    }
  }

  onTimeUpdate(e){
    //setLectureStage이랑 questionLoader로 분리해야함
    this.setState({currentTime:e.target.currentTime})
    if(!this.videoRef.current) return
    const checkPoint = lectureConfig[this.state.lectureCode].checkPoint
    var currentTime = this.videoRef.current.currentTime
    var lectureStage

    if(checkPoint[0]>currentTime) {lectureStage = 0}
    for(var i in checkPoint){
      if(checkPoint[i]<=currentTime) { lectureStage = Math.min(Number(i)+1, checkPoint.length-1) }
      if(checkPoint[i]>currentTime){ break }
    }

    this.setState({lectureStage:lectureStage})

    for(var i in checkPoint){
      if(checkPoint[i]-currentTime<0.4 && checkPoint[i]-currentTime>0){
        this.videoRef.current.pause()
        this.setState({onQuestion:true, questionStage:0, questionState:"unSubmit"})
        break;   
      }
    }
    console.log(this.state.lectureCode, this.state.questionStage)
  }

  srcUpdate(){
    var lectureRef = this.storage.ref(this.state.lectureCode + '.mp4');
    lectureRef.getDownloadURL().then((url)=>{
        this.videoRef.current.src = url;
        this.pastLectureCode = this.state.lectureCode
    }).catch(function(error) {
        console.log(error, "srcChangeError")
    })
  }

  componentDidMount(){
    this.srcUpdate()
    this.videoRef.current.addEventListener('timeupdate', (e) => {this.onTimeUpdate(e)})
    this.videoRef.current.addEventListener('loadedmetadata', ()=>{
      if(this.videoRef.current){
        this.videoRef.current.currentTime = this.startTime
        this.videoRef.current.play()
      }
    })
    this.videoRef.current.addEventListener('play', ()=>{this.playPauseRenderer();})
    this.videoRef.current.addEventListener('pause', ()=>{this.playPauseRenderer();})
    this.videoRef.current.addEventListener('volumechange', ()=>{this.volumePressRenderer();})
  }



  componentDidUpdate(){
    if(this.pastLectureCode !== this.state.lectureCode)
    this.srcUpdate()
  }


  render(){
    const subconceptList = lectureConfig[this.state.lectureCode].subconceptList[this.state.lectureStage][this.state.questionStage]
    const {questionText, answer, explonation} = questionConfig[this.state.lectureCode][this.state.lectureStage][this.state.questionStage] 
    return (
      <div className="helpoPlayer" style={{backgroundColor: "white"}}>
        <HelpoVideo ref={this.videoRef} videoSrc={this.videoSrc} />
        {/* TODO:Change questionConfig to reference question with questionCode */}
        <Question 
          handler={this.questionResponseHandler}
          questionCode={this.questionCode} 
          onQuestion={this.state.onQuestion} 
          questionState={this.state.questionState}
          solutionState={this.state.solutionState}
          onPass={this.onPass}
          onSolution={this.onSolution}
          onStageChange={this.onStageChange}
          onSubmit={this.onSubmit} 
          questionText={questionText} 
          answer={answer} 
          explonation={explonation}
        />
        <Navigator handler={this.navigatorHandler} navigatorList={this.state.navigatorList}/>
        <Subconcept handler={this.subconceptHandler} subconceptList={subconceptList}/>
      </div>
    )
  }
}

export default HelpoPlayer;