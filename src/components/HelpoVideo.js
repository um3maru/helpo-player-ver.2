import React from 'react'
import '../style/HelpoPlayer.css';

const HelpoVideo = React.forwardRef((props, ref) => {
    function getCurTime(){
        if(ref.current != null){
            let curTime = ref.current.currentTime;
            let fullTime = ref.current.duration;

            let result = ""

            let organizedFullTime = (()=>{
            let result = []
            while(fullTime >= 1){
                let timePart = parseInt(fullTime%60)
                result.push(timePart)
                fullTime = parseInt(fullTime/60)
            }
            return result
            })

            let organizedTime = (()=>{
            let result = []
            while(curTime >= 1){
                let timePart = parseInt(curTime%60)
                result.push(timePart)
                curTime = parseInt(curTime/60)
            }
            return result
            })

            let renderTime = (()=>{
            let curTime = organizedTime()
            let fullTime = organizedFullTime()
            let resultCurrentTime = ""
            let resultFullTime = ""

            for(let i=0; i<fullTime.length; i++){
                let timePart = fullTime[i].toString()
                if(i != fullTime.length - 1){
                if(timePart.length == 0) timePart = "00"
                if(timePart.length == 1) timePart = "0" + timePart
                }
                if(resultFullTime == "") resultFullTime = timePart
                else resultFullTime = timePart + ":" + resultFullTime
            }

            for(let i=0; i<fullTime.length; i++){
                let timePart
                if(i >= curTime.length) timePart = "0"
                else timePart = curTime[i].toString()
                if(i != fullTime.length - 1){
                if(timePart.length == 0) timePart = "00"
                if(timePart.length == 1) timePart = "0" + timePart
                }
                if(resultCurrentTime == "") resultCurrentTime = timePart
                else resultCurrentTime = timePart + ":" + resultCurrentTime
            }

            return resultCurrentTime + " / " + resultFullTime;
            })
            
            return renderTime()
        } else {
            return 0
        }
    }
    function playPauseHandler(){
        if(ref.current != null){
            if (ref.current.paused || ref.current.ended) {
                ref.current.play()
            }
            else {
                ref.current.pause()
            }
        }
    }

    function volumePressHandler(){
        if(ref.current != null){
            if(ref.current.muted) ref.current.muted = false;
            else ref.current.muted = true;
        }
    }

    return(
        <div>
            <video id="helpoVideo" ref={ref} src={props.videoSrc} height={window.innerHeight} controls={false}></video>
            <div className="container">
                <div id="video-controls" className="controls" data-state="visible">
                    <button id="playpause" data-state="play" onClick={playPauseHandler}></button>
                    <button id="mute" data-state="volume-on" onClick={volumePressHandler}></button>
                    <button id="time" data-state="time">{getCurTime()}</button>
                    <button id="fs" data-state="go-fullscreen"></button>
                </div>
                <div className="progress">
                    <progress id="progress" value="0" min="0">
                    <span id="progress-bar"></span>
                    </progress>
                </div>
            </div>
        </div>
    )
})


export default HelpoVideo